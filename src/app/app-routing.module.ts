import { CategorieModule } from "./DemoPages/Categorie/categorie.module";
import { Categorie } from "./Model/Categorie";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BaseLayoutComponent } from "./Layout/base-layout/base-layout.component";

const routes: Routes = [
  {
    path: "",
    component: BaseLayoutComponent,
    children: [
      {
        path: "reglement",
        loadChildren: () =>
          import("./DemoPages/Reglement/reglement.module").then(
            (m) => m.ReglementModule
          ),
      },
      {
        path: "reclamation",
        loadChildren: () =>
          import("./DemoPages/Reclamation/reclamation.module").then(
            (m) => m.ReclamationModule
          ),
      },
      {
        path: "facture",
        loadChildren: () =>
          import("./DemoPages/Facture/facture.module").then(
            (m) => m.FactureModule
          ),
      },
      {
        path: "administrateur",
        loadChildren: () =>
          import("./DemoPages/profile/profile.module").then(
            (m) => m.ProfileModule
          ),
      },
      {
        path: "utilisateurs",
        loadChildren: () =>
          import("./DemoPages/Utilisateurs/utilisateurs.module").then(
            (m) => m.UtilisateursModule
          ),
      },
      {
        path: "client",
        loadChildren: () =>
          import("./DemoPages/Clients/clients.module").then(
            (m) => m.ClientsModule
          ),
      },
      {
        path: "produit",
        loadChildren: () =>
          import("./DemoPages/Produit/add-produit.module").then(
            (m) => m.AddProduitModule
          ),
      },
      {
        path: "conge",
        loadChildren: () =>
          import("./DemoPages/congé/congé.module").then((m) => m.CongéModule),
      },
      {
        path: "categorie",
        loadChildren: () =>
          import("./DemoPages/Categorie/categorie.module").then(
            (m) => m.CategorieModule
          ),
      },
      {
        path: "catalogue",
        loadChildren: () =>
          import("./DemoPages/Catalogue/catalogue.module").then(
            (m) => m.CatalogueModule
          ),
      },
      {
        path: "charts",
        loadChildren: () =>
          import("./DemoPages/Charts/Charts.module").then((m) => m.ChartModule),
      },
      {
        path: "components",
        loadChildren: () =>
          import("./DemoPages/Components/Components.module").then(
            (m) => m.ComponentsDrawerModule
          ),
      },
      {
        path: "dashboards",
        loadChildren: () =>
          import("./DemoPages/Dashboards/Dashboards.module").then(
            (m) => m.DashboardsModule
          ),
      },
      {
        path: "commande",
        loadChildren: () =>
          import("./DemoPages/Commande/commande.module").then(
            (m) => m.CommandeModule
          ),
      },
      {
        path: "layout",
        loadChildren: () =>
          import("./DemoPages/Material/Layout/layout.module").then(
            (m) => m.MaterialLayoutModule
          ),
      },
      {
        path: "form-controls",
        loadChildren: () =>
          import("./DemoPages/Material/FormControls/formcontrols.module").then(
            (m) => m.MaterialFormControlModule
          ),
      },
      {
        path: "material",
        loadChildren: () =>
          import("./DemoPages/Material/Material.module").then(
            (m) => m.MaterialModule
          ),
      },

      {
        path: "",
        redirectTo: "dashboards/analytics",
        pathMatch: "full",
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: "enabled",
      anchorScrolling: "enabled",
      relativeLinkResolution: "legacy",
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
