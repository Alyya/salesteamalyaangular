import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { th } from "date-fns/locale";
import { environment } from "src/environments/environment";
import { TypeReglement } from "../Model/TypeReglement";

@Injectable({
  providedIn: "root",
})
export class TypeReglementService {
  private apiServerUrl = `${environment.apiBaseUrl}/TypeReglement/`;
  formData: TypeReglement;
  constructor(private http: HttpClient) {}
  getAll() {
    return this.http.get(this.apiServerUrl + "TypeReglements");
  }
  AjouterType() {
    return this.http.post(this.apiServerUrl + "add", this.formData);
  }
  UpdateType() {
    return this.http.put(
      this.apiServerUrl + "update/" + this.formData.idReglement,
      this.formData
    );
  }
  DeleteType(idReglement) {
    return this.http.delete(this.apiServerUrl + "delete/" + idReglement);
  }
}
