import { TestBed } from '@angular/core/testing';

import { TypeReglementService } from './type-reglement.service';

describe('TypeReglementService', () => {
  let service: TypeReglementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypeReglementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
