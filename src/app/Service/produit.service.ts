import { Observable } from "rxjs";
import { Produit } from "./../Model/Produit";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Injectable } from "@angular/core";
@Injectable({
  providedIn: "root",
})
export class ProduitService {
  private apiServerUrl = `${environment.apiBaseUrl}/produit`;

  constructor(private http: HttpClient) {}
  public getProduits(): Observable<Produit[]> {
    return this.http.get<Produit[]>(`${this.apiServerUrl}/produits`);
  }
  public getMeilleur(): Observable<Produit[]> {
    return this.http.get<Produit[]>(`${this.apiServerUrl}/meilleur`);
  }
  public getCount(): Observable<Produit[]> {
    return this.http.get<Produit[]>(`${this.apiServerUrl}/count`);
  }
  public getMauvais(): Observable<Produit[]> {
    return this.http.get<Produit[]>(`${this.apiServerUrl}/mauvais`);
  }
  getById(idProduit): Observable<Produit> {
    return this.http.get<Produit>(`${this.apiServerUrl}/${idProduit}`);
  }
  public addProduit(Produit: Produit): Observable<Produit> {
    return this.http.post<Produit>(`${this.apiServerUrl}/add/`, Produit);
  }
  public updateProduit(produit) {
    return this.http.put(`${this.apiServerUrl}/update`, produit);
  }
  public deleteProduit(idProduit) {
    return this.http.delete(
      `${this.apiServerUrl}/delete?idProduit=` + idProduit
    );
  }
}
