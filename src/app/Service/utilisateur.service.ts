import { Utilisateur } from "./../Model/Utilisateur";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class UtilisateurService {
  private apiServerUrl = `${environment.apiBaseUrl}/user/`;
  formData: Utilisateur;
  constructor(private http: HttpClient) {}
  getAll() {
    return this.http.get(this.apiServerUrl + "users");
  }
  getAllCommerciaux() {
    return this.http.get(this.apiServerUrl + "commerciaux");
  }

  getBestC() {
    return this.http.get(this.apiServerUrl + "BestC");
  }

  getbadC() {
    return this.http.get(this.apiServerUrl + "BadC");
  }
  public getCountComm(): Observable<Utilisateur[]> {
    return this.http.get<Utilisateur[]>(`${this.apiServerUrl}countCom`);
  }
  public getCountAd(): Observable<Utilisateur[]> {
    return this.http.get<Utilisateur[]>(`${this.apiServerUrl}countAd`);
  }
  getAllAdmins() {
    return this.http.get(this.apiServerUrl + "admins");
  }
  getCinq() {
    return this.http.get(this.apiServerUrl + "cinqUser");
  }
  AjouterUser(user: any) {
    return this.http.post(this.apiServerUrl + "add", user);
  }
  UpdateUser() {
    return this.http.put(
      this.apiServerUrl + "update/" + this.formData.idUser,
      this.formData
    );
  }
  DeleteUser(idUser) {
    return this.http.delete(this.apiServerUrl + "delete/" + idUser);
  }
  getNombreUser() {
    //  return this.http.get
  }
}
