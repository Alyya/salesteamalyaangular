import { Client } from "./../Model/Client";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ClientService {
  private apiServerUrl = `${environment.apiBaseUrl}/client`;

  constructor(private http: HttpClient) {}
  getAllClient() {
    return this.http.get(`${this.apiServerUrl}/clients`);
  }

  getBestC() {
    return this.http.get(`${this.apiServerUrl}/BestCl`);
  }

  getbadC() {
    return this.http.get(`${this.apiServerUrl}/badCl`);
  }
  public getCount(): Observable<Client[]> {
    return this.http.get<Client[]>(`${this.apiServerUrl}/count`);
  }
}
