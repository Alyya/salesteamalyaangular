import { Categorie } from "./../Model/Categorie";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class CategorieService {
  private apiServerUrl = `${environment.apiBaseUrl}/categorie`;

  constructor(private http: HttpClient) {}

  public getCategories(): Observable<Categorie[]> {
    return this.http.get<Categorie[]>(`${this.apiServerUrl}/categories`);
  }

  getById(id): Observable<Categorie> {
    return this.http.get<Categorie>(`${this.apiServerUrl}/${id}`);
  }
  public addCategorie(categorie: Categorie): Observable<Categorie> {
    return this.http.post<Categorie>(`${this.apiServerUrl}/add`, categorie);
  }
  ////categorie:Categorie
  public updateCategorie(categorie) {
    return this.http.put(`${this.apiServerUrl}/update`, categorie);
  }
  public deleteCategorie(idCategorie) {
    return this.http.delete(
      `${this.apiServerUrl}/delete?idCategorie=` + idCategorie
    );
  }
}
