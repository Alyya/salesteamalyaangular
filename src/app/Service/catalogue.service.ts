import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Catalogue } from "../Model/Catalogue";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class CatalogueService {
  //private apiServerUrl = environment.apiBaseUrl;
  private apiServerUrl = `${environment.apiBaseUrl}/catalogue`;

  constructor(private http: HttpClient) {}
  public getCatalogues(): Observable<Catalogue[]> {
    return this.http.get<Catalogue[]>(`${this.apiServerUrl}/catalogues`);
  }
  getById(id): Observable<Catalogue> {
    return this.http.get<Catalogue>(`${this.apiServerUrl}/${id}`);
  }
  /*public addCatalogue(Catalogue: Catalogue, idUser): Observable<Catalogue> {
    return this.http.post<Catalogue>(
      `$this.apiServerUrl}Catalogue/add/` + idUser,
      Catalogue
    );
  }*/

  public addCatalogue(catalogue: Catalogue): Observable<Catalogue> {
    return this.http.post<Catalogue>(`${this.apiServerUrl}/add`, catalogue);
  }
  public updateCatalogue(catalogue) {
    return this.http.put(`${this.apiServerUrl}/update`, catalogue);
  }
  public deleteCatalogue(idCatalogue) {
    return this.http.delete(
      `${this.apiServerUrl}/delete?idCatalogue=` + idCatalogue
    );
  }
}
