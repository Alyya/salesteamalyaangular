import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { congé } from "../Model/congé";

@Injectable({
  providedIn: "root",
})
export class CongeService {
  private apiServerUrl = `${environment.apiBaseUrl}/conge`;

  constructor(private http: HttpClient) {}

  public getcongés(): Observable<congé[]> {
    return this.http.get<congé[]>(`${this.apiServerUrl}/conges`);
  }

  getById(id): Observable<congé> {
    return this.http.get<congé>(`${this.apiServerUrl}/${id}`);
  }
  public addcongé(congé: congé): Observable<congé> {
    return this.http.post<congé>(`${this.apiServerUrl}/add`, congé);
  }
  ////congé:congé
  public updatecongé(congé) {
    return this.http.put(`${this.apiServerUrl}/update`, congé);
  }
  public deletecongé(idconge) {
    return this.http.delete(`${this.apiServerUrl}/delete?idconge=` + idconge);
  }
}
