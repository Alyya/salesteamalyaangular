import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Commande } from "../Model/Commande";

@Injectable({
  providedIn: "root",
})
export class CommandeService {
  private apiServerUrl = `${environment.apiBaseUrl}/commande`;

  constructor(private http: HttpClient) {}

  public getCommandes(): Observable<Commande[]> {
    return this.http.get<Commande[]>(`${this.apiServerUrl}/commandes`);
  }
  public getCount(): Observable<Commande[]> {
    return this.http.get<Commande[]>(`${this.apiServerUrl}/count`);
  }

  public getCmds(): Observable<Commande[]> {
    return this.http.get<Commande[]>(`${this.apiServerUrl}/cmds`);
  }

  getById(id): Observable<Commande> {
    return this.http.get<Commande>(`${this.apiServerUrl}/${id}`);
  }
  public addCommande(categorie: Commande): Observable<Commande> {
    return this.http.post<Commande>(`${this.apiServerUrl}/add`, categorie);
  }
  ////categorie:Categorie
  public updateCommande(commande) {
    return this.http.put(`${this.apiServerUrl}/update`, commande);
  }
  public deleteCommande(idCommande) {
    return this.http.delete(
      `${this.apiServerUrl}/delete?idCommande=` + idCommande
    );
  }
}
