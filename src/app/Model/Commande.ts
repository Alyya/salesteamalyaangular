export class Commande {
  idCommande: number;
  numCmd: number;
  etatCmd: String;
  passedAt: Date;
  prixHT: number;
  prixTVA: number;
  prixTTC: number;
  total: number;
  Clients: String;
}
