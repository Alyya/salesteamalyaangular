export interface Produit {
  idProduit: number;
  codeProduit: string;
  nomProduit: string;
  Categories: string;
  dateCreation: Date;
  Reference: string;
  fileName: string;
  typeProduit: string;
  Designiation: string;
  prixTTC: number;
  prixtva: number;
  prixtht: number;
  poids: number;
  dateExpiration: Date;
}
