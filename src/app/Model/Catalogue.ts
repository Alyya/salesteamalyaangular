import { FormArray } from "@angular/forms";
export interface Catalogue {
  idCatalogue: number;
  nomCatalogues: string;
  idUser: number;
  idProduit: number;
  description: string;
  prod: FormArray;
}
