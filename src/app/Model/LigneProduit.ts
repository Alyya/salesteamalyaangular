export interface LigneProduit {
  idProduit: number;
  codeProduit: string;
  Categories: string;
  dateCreation: Date;
  Designiation: string;
  prixttc: number;
}
