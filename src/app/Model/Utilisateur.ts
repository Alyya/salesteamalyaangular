import { FormArray } from "@angular/forms";
export class Utilisateur {
  idUser: number;
  adresse: string;
  deleted: number;
  email: string;
  nom: string;
  prenom: string;
  rolename: string;
  username: string;
  password: string;
  codePostal: string;
  tel: number;
  catalogue: FormArray;
}
