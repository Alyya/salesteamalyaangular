export class Client {
  idClient: Number;
  nom: String;
  prenom: String;
  email: String;
  adresse: String;
  pattente: String;
  tel: String;
  Fax: String;
  nomCommerciale: String;
  avatar: String;
}
