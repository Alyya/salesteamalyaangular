import { ListReglementComponent } from "./list-reglement.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: ListReglementComponent,
    data: {
      title: "List-Reglement",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListReglementRoutingModule {}
