import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListReglementRoutingModule } from './list-reglement-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ListReglementRoutingModule
  ]
})
export class ListReglementModule { }
