import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Reglement",
      status: "false",
    },
    children: [
      {
        path: "listReglement",
        loadChildren: () =>
          import("./list-reglement/list-reglement.module").then(
            (m) => m.ListReglementModule
          ),
      },
      {
        path: "type",
        loadChildren: () =>
          import("./type-reglement/type-reglement.module").then(
            (m) => m.TypeReglementModule
          ),
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReglementRoutingModule {}
