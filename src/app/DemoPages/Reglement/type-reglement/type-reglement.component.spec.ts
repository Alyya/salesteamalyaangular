import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeReglementComponent } from './type-reglement.component';

describe('TypeReglementComponent', () => {
  let component: TypeReglementComponent;
  let fixture: ComponentFixture<TypeReglementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeReglementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeReglementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
