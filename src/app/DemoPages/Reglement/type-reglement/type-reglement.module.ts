import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TypeReglementRoutingModule } from './type-reglement-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TypeReglementRoutingModule
  ]
})
export class TypeReglementModule { }
