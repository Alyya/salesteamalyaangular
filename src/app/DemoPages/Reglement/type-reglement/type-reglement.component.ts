import { Categorie } from "./../../../Model/Categorie";
import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { TypeReglementService } from "src/app/Service/type-reglement.service";
import { TypeReglement } from "../../../Model/TypeReglement";

@Component({
  selector: "app-type-reglement",
  templateUrl: "./type-reglement.component.html",
  styleUrls: ["./type-reglement.component.sass"],
})
export class TypeReglementComponent implements OnInit {
  TypeList: {};
  constructor(
    private toastr: ToastrService,
    public service: TypeReglementService
  ) {}

  ngOnInit(): void {
    this.resetForm();
    this.getAll();
  }
  resetForm(form?: NgForm) {
    if (form != null) form.resetForm();
    this.service.formData = {
      idReglement: 0,
      typeName: "",
    };
  }
  getAll() {
    this.service.getAll().subscribe((data) => {
      this.TypeList = data as TypeReglement[];
    });
  }
  onsubmit(form: NgForm) {
    if (this.service.formData.idReglement == 0) {
      this.AjouterType();
    } else {
      this.updateType();
    }
    this.resetForm();
  }
  AjouterType() {
    this.service.AjouterType().subscribe(
      (res: any) => {
        this.toastr.success("ajout avec succes", "Sucess");
        this.getAll();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  updateType() {
    this.service.UpdateType().subscribe(
      (res: any) => {
        this.toastr.info("Type modifiée", "Sucess");
        this.getAll();
      },
      (err) => {
        console.log(err);
      }
    );
  }
  ToForm(type: TypeReglement) {
    this.service.formData = Object.assign({}, type);
  }
}
