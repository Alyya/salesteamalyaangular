import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ReglementRoutingModule } from "./reglement-routing.module";

@NgModule({
  declarations: [],
  imports: [CommonModule, ReglementRoutingModule],
})
export class ReglementModule {}
