import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Material Form Control",
      status: false,
    },
    children: [
      {
        path: "radio",
        loadChildren: () =>
          import("./radio/radio.module").then((m) => m.RadioModule),
      },
      {
        path: "datepicker",
        loadChildren: () =>
          import("./mat-datepicker/mat-datepicker.module").then(
            (m) => m.DatepickerModule
          ),
      },
      {
        path: "form-field",
        loadChildren: () =>
          import("./form-field/form-field.module").then(
            (m) => m.FormFieldModule
          ),
      },
      {
        path: "input",
        loadChildren: () =>
          import("./input/input.module").then((m) => m.InputModule),
      },
      {
        path: "select",
        loadChildren: () =>
          import("./select/select.module").then((m) => m.SelectModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MaterialFormControlRoutingModule {}
