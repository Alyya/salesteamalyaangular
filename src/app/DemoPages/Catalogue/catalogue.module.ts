import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CatalogueRoutingModule } from "./catalogue-routing.module";
import { CatalogueDetailsComponent } from './catalogue-details/catalogue-details.component';

@NgModule({
  declarations: [CatalogueDetailsComponent],
  imports: [CommonModule, CatalogueRoutingModule],
})
export class CatalogueModule {}
