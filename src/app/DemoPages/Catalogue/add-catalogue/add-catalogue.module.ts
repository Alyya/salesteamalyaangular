import { AddCatalogueComponent } from "./add-catalogue.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AddCatalogueRoutingModule } from "./add-catalogue-routing.module";

@NgModule({
  declarations: [AddCatalogueComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    NgbModule,
    AddCatalogueRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class AddCatalogueModule {}
