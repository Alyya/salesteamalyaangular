import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddCatalogueComponent } from "./add-catalogue.component";

const routes: Routes = [
  {
    path: "",
    component: AddCatalogueComponent,
    data: {
      title: "Add-Catalogue",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddCatalogueRoutingModule {}
