import { CatalogueService } from "./../../../Service/catalogue.service";
import { Catalogue } from "./../../../Model/Catalogue";
import { LigneProduit } from "./../../../Model/LigneProduit";
import { ProduitService } from "./../../../Service/produit.service";
import { ViewChild, Component, OnInit } from "@angular/core";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { faTags } from "@fortawesome/free-solid-svg-icons";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Produit } from "src/app/Model/Produit";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormGroupDirective,
  FormControl,
} from "@angular/forms";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-add-catalogue",
  templateUrl: "./add-catalogue.component.html",
  styleUrls: ["./add-catalogue.component.sass"],
})
export class AddCatalogueComponent implements OnInit {
  heading = "Modals";
  subheading =
    "Wide selection of modal dialogs styles and animations available.";
  icon = "pe-7s-phone icon-gradient bg-premium-dark";

  closeResult: string;

  toggleMobileSidebar: any;
  faStar = faStar;
  faPlus = faPlus;
  faAngleDown = faAngleDown;
  faSearch = faSearch;
  faTags = faTags;
  faCalendarAlt = faCalendarAlt; //-----------------//
  public produits: Produit[] = [];
  /****addprodcata */
  public prods: Produit[] = [];
  public catalogues: Catalogue[] = [];
  public cat: Catalogue[] = [];

  productForm = new FormGroup({
    nomCatalogues: new FormControl("", Validators.required),
    description: new FormControl("", Validators.required),
    prod: new FormControl("", Validators.required),
  });

  //-----------------//
  form!: FormGroup;
  newProd = {
    idProduit: 0,
    codeProduit: "",
    nom: "",
    Categories: "",
    Reference: "",
    avatar: "",
    typeProduit: "",
    Designiation: "",
    prixttc: 0,
    prixtva: 0,
    prixtht: 0,
    poids: 0,
    dateExpiration: new Date(),
    dateCreation: new Date(),
  };
  /********* */
  prodList = [];
  clicked = false;
  /***/
  catalogueForm = new FormGroup({
    nomCatalogues: new FormControl(""),
    description: new FormControl(""),
    prod: new FormControl(""),
  });
  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private produitService: ProduitService,
    private catalogueService: CatalogueService,
    private http: HttpClient,
    private toastr: ToastrService
  ) {
    this.produits = [];
    this.catalogues = [];
    this.getProduits();
    this.productForm = this.fb.group({
      nomCatalogues: "",
      description: "",
      prod: this.fb.array([]),
    });
  }
  prod(): FormArray {
    return this.productForm.get("prod") as FormArray;
  }

  openLg(content) {
    this.modalService.open(content, { size: "lg" });
  }

  open(content) {
    this.modalService.open(content).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  actionMethod() {
    //event.target.disabled = true;
    console.log("actionMethod was called!");
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  ngOnInit(): void {
    // this.getProduits();
  }
  submit() {
    console.log("submitted");
  }
  public getProduits(): void {
    this.produitService.getProduits().subscribe(
      (response: Produit[]) => {
        this.produits = response;
        // console.log(this.produits);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  /* getProd(produit: Produit) {
    this.newProd = produit;
  }*/
  public addProdToCatalogue(pro) {
    //console.log(this.prods);
    this.prodList.push(pro);
    console.log(this.prodList);

    // alert(this.prodList);

    //this.prods.push(this.prodss);
  }
  ondeleteL(item) {
    this.prodList.splice(item, 1);
  }
  reset() {
    this.catalogueForm.setValue({
      nomCatalogues: "",
      description: "",
    });
  }
  onSubmit() {
    //console.log(this.productForm.value);
    this.productForm.value.prod = this.prodList;
    // console.log(this.prodList);
    console.log(this.productForm.value);
    this.catalogueService
      .addCatalogue(this.productForm.value)
      .subscribe((response: Catalogue) => {
        console.log(response);
        this.getCatalogues();
        this.productForm.reset();
        this.toastr.success("New row added successfully", "New Row");
      });
  }
  getCatalogues() {}
}
