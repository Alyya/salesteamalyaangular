import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Catalogue",
      status: "false",
    },
    children: [
      {
        path: "ajouterCatalogue",
        loadChildren: () =>
          import("../Catalogue/add-catalogue/add-catalogue.module").then(
            (m) => m.AddCatalogueModule
          ),
      },
      {
        path: "CatalogueDetail",
        loadChildren: () =>
          import(
            "../Catalogue/catalogue-details/catalogue-details.module"
          ).then((m) => m.CatalogueDetailsModule),
      },
      {
        path: "listCatalogue",
        loadChildren: () =>
          import("../Catalogue/list-catalogue/list-catalogue.module").then(
            (m) => m.ListCatalogueModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatalogueRoutingModule {}
