import { ListCatalogueComponent } from "./list-catalogue.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: ListCatalogueComponent,
    data: {
      title: "List-Catalogue",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListCatalogueRoutingModule {}
