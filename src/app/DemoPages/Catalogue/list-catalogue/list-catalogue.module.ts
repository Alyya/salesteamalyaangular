import { ListCatalogueComponent } from "./list-catalogue.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { ListCatalogueRoutingModule } from "./list-catalogue-routing.module";
@NgModule({
  declarations: [ListCatalogueComponent],
  imports: [
    CommonModule,
    ListCatalogueRoutingModule,
    FormsModule,
    FontAwesomeModule,
    NgbModule,
  ],
})
export class ListCatalogueModule {}
