import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Catalogue } from "src/app/Model/Catalogue";
import { CatalogueService } from "src/app/Service/catalogue.service";

@Component({
  selector: "app-list-catalogue",
  templateUrl: "./list-catalogue.component.html",
  styleUrls: ["./list-catalogue.component.sass"],
})
export class ListCatalogueComponent implements OnInit {
  constructor(
    private modalService: NgbModal,
    private catService: CatalogueService
  ) {}
  public cats: Catalogue[] = [];

  closeResult: string;
  open(content) {
    this.modalService.open(content).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  public getAllCatalogues(): void {
    this.catService.getCatalogues().subscribe(
      (response: Catalogue[]) => {
        this.cats = response;
        console.log(this.cats);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  ngOnInit(): void {
    this.getAllCatalogues();
  }
}
