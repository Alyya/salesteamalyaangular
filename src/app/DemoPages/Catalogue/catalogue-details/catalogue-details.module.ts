import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogueDetailsRoutingModule } from './catalogue-details-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CatalogueDetailsRoutingModule
  ]
})
export class CatalogueDetailsModule { }
