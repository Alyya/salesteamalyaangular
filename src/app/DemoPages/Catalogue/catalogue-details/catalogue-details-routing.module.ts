import { CatalogueDetailsComponent } from "./catalogue-details.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: CatalogueDetailsComponent,
    data: {
      title: "Detail-Catalogue",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatalogueDetailsRoutingModule {}
