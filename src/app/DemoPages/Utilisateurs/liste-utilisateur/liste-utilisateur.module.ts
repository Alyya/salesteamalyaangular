import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ListeUtilisateurRoutingModule } from "./liste-utilisateur-routing.module";

@NgModule({
  declarations: [],
  imports: [CommonModule, ListeUtilisateurRoutingModule],
})
export class ListeUtilisateurModule {}
