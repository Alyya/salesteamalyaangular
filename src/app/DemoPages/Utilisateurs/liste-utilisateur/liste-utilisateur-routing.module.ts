import { ListeUtilisateurComponent } from "./liste-utilisateur.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: ListeUtilisateurComponent,
    data: {
      title: "List-Utilisateurs",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListeUtilisateurRoutingModule {}
