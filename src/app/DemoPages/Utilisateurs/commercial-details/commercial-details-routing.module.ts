import { CommercialDetailsComponent } from "./commercial-details.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: CommercialDetailsComponent,
    data: {
      title: "Commercial-Details",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommercialDetailsRoutingModule {}
