import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommercialDetailsRoutingModule } from './commercial-details-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CommercialDetailsRoutingModule
  ]
})
export class CommercialDetailsModule { }
