import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Utilisateur",
      status: "false",
    },
    children: [
      {
        path: "ajouterUser",
        loadChildren: () =>
          import("./ajouter-user/ajouter-user.module").then(
            (m) => m.AjouterUserModule
          ),
      },
      {
        path: "listUser",
        loadChildren: () =>
          import("./liste-utilisateur/liste-utilisateur.module").then(
            (m) => m.ListeUtilisateurModule
          ),
      },
      {
        path: "detailCommercial",
        loadChildren: () =>
          import("./commercial-details/commercial-details.module").then(
            (m) => m.CommercialDetailsModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UtilisateursRoutingModule {}
