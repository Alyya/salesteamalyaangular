import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { UtilisateursRoutingModule } from "./utilisateurs-routing.module";
import { CommercialDetailsComponent } from "./commercial-details/commercial-details.component";

@NgModule({
  declarations: [CommercialDetailsComponent],
  imports: [CommonModule, UtilisateursRoutingModule],
})
export class UtilisateursModule {}
