import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AjouterUserRoutingModule } from './ajouter-user-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AjouterUserRoutingModule
  ]
})
export class AjouterUserModule { }
