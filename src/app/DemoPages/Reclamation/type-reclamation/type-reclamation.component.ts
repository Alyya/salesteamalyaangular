import { Component, OnInit } from "@angular/core";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { faTags } from "@fortawesome/free-solid-svg-icons";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { HttpErrorResponse } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";

import {
  NgForm,
  FormGroup,
  AbstractControlOptions,
  FormBuilder,
  Validators,
} from "@angular/forms";

@Component({
  selector: "app-type-reclamation",
  templateUrl: "./type-reclamation.component.html",
  styleUrls: ["./type-reclamation.component.sass"],
})
export class TypeReclamationComponent implements OnInit {
  heading = "Modals";
  subheading =
    "Wide selection of modal dialogs styles and animations available.";
  icon = "pe-7s-phone icon-gradient bg-premium-dark";

  closeResult: string;

  toggleMobileSidebar: any;
  faStar = faStar;
  faPlus = faPlus;
  faAngleDown = faAngleDown;
  faSearch = faSearch;
  faTags = faTags;
  faCalendarAlt = faCalendarAlt;

  constructor() {}

  ngOnInit(): void {}
}
