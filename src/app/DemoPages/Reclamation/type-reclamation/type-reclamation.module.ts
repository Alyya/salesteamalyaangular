import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TypeReclamationRoutingModule } from './type-reclamation-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TypeReclamationRoutingModule
  ]
})
export class TypeReclamationModule { }
