import { TypeReclamationComponent } from "./type-reclamation.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: TypeReclamationComponent,
    data: {
      title: "Type-Reclamation",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TypeReclamationRoutingModule {}
