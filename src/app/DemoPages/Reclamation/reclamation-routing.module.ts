import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "ListeReclamation",
      status: "false",
    },
    children: [
      {
        path: "listReclamation",
        loadChildren: () =>
          import("./list-reclamation/list-reclamation.module").then(
            (m) => m.ListReclamationModule
          ),
      },
      {
        path: "type",
        loadChildren: () =>
          import("./type-reclamation/type-reclamation.module").then(
            (m) => m.TypeReclamationModule
          ),
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReclamationRoutingModule {}
