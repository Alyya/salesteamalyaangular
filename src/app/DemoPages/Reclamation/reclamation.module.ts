import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ReclamationRoutingModule } from "./reclamation-routing.module";
import { TypeReclamationComponent } from './type-reclamation/type-reclamation.component';

@NgModule({
  declarations: [TypeReclamationComponent],
  imports: [CommonModule, ReclamationRoutingModule],
})
export class ReclamationModule {}
