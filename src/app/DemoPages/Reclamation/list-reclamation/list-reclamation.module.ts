import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListReclamationRoutingModule } from './list-reclamation-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ListReclamationRoutingModule
  ]
})
export class ListReclamationModule { }
