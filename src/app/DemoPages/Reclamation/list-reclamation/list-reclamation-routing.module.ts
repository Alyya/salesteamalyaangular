import { ListReclamationComponent } from "./list-reclamation.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
const routes: Routes = [
  {
    path: "",
    component: ListReclamationComponent,
    data: {
      title: "ListReclamation",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListReclamationRoutingModule {}
