import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileAdRoutingModule } from './profile-ad-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProfileAdRoutingModule
  ]
})
export class ProfileAdModule { }
