import { ProfileAdComponent } from "./profile-ad.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
const routes: Routes = [
  {
    path: "",
    component: ProfileAdComponent,
    data: {
      title: "Profile",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileAdRoutingModule {}
