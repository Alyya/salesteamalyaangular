import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Profile",
      status: "false",
    },
    children: [
      {
        path: "profile",
        loadChildren: () =>
          import("./profile-ad/profile-ad.module").then(
            (m) => m.ProfileAdModule
          ),
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
