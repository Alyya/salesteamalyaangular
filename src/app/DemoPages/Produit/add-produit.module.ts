import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AddProduitRoutingModule } from "./add-produit-routing.module";
import { DetailProdComponent } from './detail-prod/detail-prod.component';

@NgModule({
  declarations: [DetailProdComponent],
  imports: [CommonModule, AddProduitRoutingModule],
})
export class AddProduitModule {}
