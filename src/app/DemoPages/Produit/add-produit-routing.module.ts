import { DetailProdModule } from "./detail-prod/detail-prod.module";
import { DetailProdComponent } from "./detail-prod/detail-prod.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Produit",
      status: "false",
    },
    children: [
      {
        path: "ajouterProduit",
        loadChildren: () =>
          import("../Produit/add-produit/add-produit.module").then(
            (m) => m.AddProduitModule
          ),
      },
      {
        path: "detailProduit",
        loadChildren: () =>
          import("../Produit/detail-prod/detail-prod.module").then(
            (m) => m.DetailProdModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddProduitRoutingModule {}
