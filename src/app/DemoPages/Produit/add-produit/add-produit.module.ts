import { AddProduitComponent } from "./add-produit.component";
import { NgModule } from "@angular/core";
import { AddProduitRoutingModule } from "./add-produit-routing.module";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AddProduitRoutingModule,
    FormsModule,
    FontAwesomeModule,
    NgbModule,
  ],
})
export class AddProduitModule {}
