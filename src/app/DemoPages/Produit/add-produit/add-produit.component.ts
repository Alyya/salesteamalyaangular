import { Catalogue } from "./../../../Model/Catalogue";
import { UtilisateurService } from "./../../../Service/utilisateur.service";
import { Utilisateur } from "./../../../Model/Utilisateur";
import { Component, OnInit } from "@angular/core";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { faTags } from "@fortawesome/free-solid-svg-icons";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { HttpErrorResponse } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { FormArray } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { FormControl } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";

import {
  NgForm,
  FormGroup,
  AbstractControlOptions,
  FormBuilder,
  Validators,
} from "@angular/forms";
import { RouteConfigLoadEnd } from "@angular/router";
import { CatalogueService } from "src/app/Service/catalogue.service";
@Component({
  selector: "app-add-produit",
  templateUrl: "./add-produit.component.html",
  styleUrls: ["./add-produit.component.sass"],
})
export class AddProduitComponent implements OnInit {
  heading = "Modals";
  subheading =
    "Wide selection of modal dialogs styles and animations available.";
  icon = "pe-7s-phone icon-gradient bg-premium-dark";

  closeResult: string;

  toggleMobileSidebar: any;
  faStar = faStar;
  faPlus = faPlus;
  faAngleDown = faAngleDown;
  faSearch = faSearch;
  faTags = faTags;
  faCalendarAlt = faCalendarAlt;
  form!: FormGroup;
  appuser: Utilisateur = new Utilisateur();
  /**** */
  formm = new FormGroup({
    username: new FormControl("", Validators.required),
    nom: new FormControl("", Validators.required),
    tel: new FormControl("", Validators.required),
    catalogue: new FormControl("", Validators.required),
    prenom: new FormControl("", Validators.required),
    adresse: new FormControl("", Validators.required),
    rolename: new FormControl("", Validators.required),
    description: new FormControl("", Validators.required),
    codePostal: new FormControl("", Validators.required),
    email: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl("", [
      Validators.required,
      Validators.minLength(8),
    ]),
  });
  public catalogues: Catalogue[] = [];
  public users: Utilisateur[] = [];

  constructor(
    private catalogueService: CatalogueService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private userService: UtilisateurService
  ) {}

  public userForm: FormGroup = this.fb.group({
    rolename: ["Commercial"],
    idUser: [""],
    adresse: [""],
    email: [""],
    deleted: [""],
    description: [""],
    codePostal: [""],
    nom: [""],
    tel: [""],
    prenom: [""],
    username: [""],
    password: [""],
    catalogue: [""],
  });

  open(content) {
    this.modalService.open(content).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  get username() {
    return this.formm.get("username");
  }
  ngOnInit() {
    this.getallCatalogue();
    this.getallUsers();
  }
  getallCatalogue(): void {
    this.catalogueService.getCatalogues().subscribe(
      (response: Catalogue[]) => {
        this.catalogues = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  getallUsers(): void {
    this.userService.getCinq().subscribe(
      (response: Utilisateur[]) => {
        this.users = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  onSubmit() {
    console.log(this.formm.value);
    this.appuser.nom = this.formm.value.nom;
    this.appuser.prenom = this.formm.value.prenom;
    this.appuser.email = this.formm.value.email;
    this.appuser.codePostal = this.formm.value.codePostal;
    this.appuser.password = this.formm.value.password;
    this.appuser.rolename = this.formm.value.rolename;
    this.appuser.adresse = this.formm.value.adresse;
    this.appuser.username = this.formm.value.username;
    this.appuser.tel = this.formm.value.tel;
    let xx = this.formm.value.catalogue;
    let yy = { idCatalogue: xx };
    this.appuser.catalogue = yy as any;
    console.log(this.appuser.catalogue);
    console.log(this.formm.value.nom);
    this.userService.AjouterUser(this.appuser).subscribe(
      (response: Utilisateur) => {
        console.log(response);
        //this.getUser();
        this.formm.reset();
        this.toastr.success("Ajout avec sucess", "Nouvel Utilisateur");
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        this.formm.reset();
      }
    );
  }
  reset() {
    this.formm.setValue({
      username: "",
      email: "",
      nom: "",
      prenom: "",
      adresse: "",
      codePostal: "",
      tel: "",
      description: "",
      catalogue: "",
      password: "",
      rolename: "",
    });
  }
}
