import { AddProduitComponent } from "./add-produit.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: AddProduitComponent,
    data: {
      title: "Add-Produit",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddProduitRoutingModule {}
