import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailProdRoutingModule } from './detail-prod-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DetailProdRoutingModule
  ]
})
export class DetailProdModule { }
