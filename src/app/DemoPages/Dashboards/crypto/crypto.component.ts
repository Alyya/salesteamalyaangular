import { ClientService } from "./../../../Service/client.service";
import { Client } from "./../../../Model/Client";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { faAngleUp } from "@fortawesome/free-solid-svg-icons";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { CommandeService } from "src/app/Service/commande.service";
import { ProduitService } from "src/app/Service/produit.service";
import { UtilisateurService } from "src/app/Service/utilisateur.service";

@Component({
  selector: "app-crypto",
  templateUrl: "./crypto.component.html",
  styles: [],
})
export class CryptoComponent implements OnInit {
  faAngleUp = faAngleUp;
  faArrowRight = faArrowRight;
  faArrowLeft = faArrowLeft;

  heading = " Dashboard Client";
  subheading = "";
  icon = "pe-7s-plane icon-gradient bg-tempting-azure";

  public meilleur: Client[] = [];
  public mauvais: Client[] = [];
  nombreClient: any;
  nombreCommerciaux: any;
  nombreCommandes: any;
  constructor(
    private modalService: NgbModal,
    private produitService: ProduitService,
    private userService: UtilisateurService,
    private cmdService: CommandeService,
    private clientService: ClientService,
    private http: HttpClient
  ) {}

  public getNombreCommerciaux(): void {
    this.userService.getCountComm().subscribe(
      (data) => {
        this.nombreCommerciaux = data;
        console.log(this.nombreCommerciaux);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getNombreCommandes(): void {
    this.cmdService.getCount().subscribe(
      (data) => {
        this.nombreCommandes = data;
        console.log(this.nombreCommandes);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getNombreClients(): void {
    this.clientService.getCount().subscribe(
      (data) => {
        this.nombreClient = data;
        console.log(this.nombreClient);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getMauvaisClients(): void {
    this.clientService.getbadC().subscribe(
      (response: Client[]) => {
        this.mauvais = response;
        console.log(this.mauvais);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  public getMeilleurClients(): void {
    this.clientService.getBestC().subscribe(
      (response: Client[]) => {
        this.meilleur = response;
        console.log(this.meilleur);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  ngOnInit() {
    this.getMeilleurClients();
    this.getMauvaisClients();
    this.getNombreClients();
    this.getNombreCommandes();
    this.getNombreCommerciaux();
  }
}
