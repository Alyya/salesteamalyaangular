import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Dashboards",
      status: false,
    },
    children: [
      {
        path: "analytics",
        loadChildren: () =>
          import("./analytics/analytics.module").then((m) => m.AnalyticsModule),
      },
      {
        path: "management",
        loadChildren: () =>
          import("./management/management.module").then(
            (m) => m.ManagementModule
          ),
      },
      {
        path: "dashClient",
        loadChildren: () =>
          import("./crypto/crypto.module").then((m) => m.CryptoModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardsRoutingModule {}
