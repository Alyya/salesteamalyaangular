import { Component, OnInit } from "@angular/core";
import { Color } from "ng2-charts";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { faAngleUp } from "@fortawesome/free-solid-svg-icons";
import { faDotCircle } from "@fortawesome/free-solid-svg-icons";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { CommandeService } from "src/app/Service/commande.service";
import { ProduitService } from "src/app/Service/produit.service";
import { UtilisateurService } from "src/app/Service/utilisateur.service";
import { Utilisateur } from "src/app/Model/Utilisateur";

@Component({
  selector: "app-management",
  templateUrl: "./management.component.html",
  styles: [],
})
export class ManagementComponent implements OnInit {
  faAngleDown = faAngleDown;
  faAngleUp = faAngleUp;
  faDotCircle = faDotCircle;
  faArrowLeft = faArrowLeft;

  heading = "Dashboard Commerciaux";
  subheading = "";
  icon = "pe-7s-user icon-gradient bg-mean-fruit";

  public bestC: Utilisateur[] = [];
  public badC: Utilisateur[] = [];

  nombreProduits: any;
  nombreCommerciaux: any;
  nombreCommandes: any;

  constructor(
    private modalService: NgbModal,
    private produitService: ProduitService,
    private userService: UtilisateurService,
    private cmdService: CommandeService,
    private http: HttpClient
  ) {}

  public getNombreCommerciaux(): void {
    this.userService.getCountComm().subscribe(
      (data) => {
        this.nombreCommerciaux = data;
        console.log(this.nombreCommerciaux);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getNombreCommandes(): void {
    this.cmdService.getCount().subscribe(
      (data) => {
        this.nombreCommandes = data;
        console.log(this.nombreCommandes);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getNombreProduits(): void {
    this.produitService.getCount().subscribe(
      (data) => {
        this.nombreProduits = data;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  public getBadC(): void {
    this.userService.getbadC().subscribe(
      (response: Utilisateur[]) => {
        this.badC = response;
        console.log(this.badC);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getBestC(): void {
    this.userService.getBestC().subscribe(
      (response: Utilisateur[]) => {
        this.bestC = response;
        console.log(this.bestC);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  ngOnInit() {
    this.getNombreProduits();
    this.getNombreCommandes();
    this.getNombreCommerciaux();
    this.getBestC();
    this.getBadC();
  }
}
