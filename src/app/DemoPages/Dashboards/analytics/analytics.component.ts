import { CommandeService } from "./../../../Service/commande.service";
import { UtilisateurService } from "./../../../Service/utilisateur.service";
import { Produit } from "./../../../Model/Produit";
import { Component, OnInit } from "@angular/core";
import { Color } from "ng2-charts/ng2-charts";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { faAngleUp } from "@fortawesome/free-solid-svg-icons";
import { faTh } from "@fortawesome/free-solid-svg-icons";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ProduitService } from "src/app/Service/produit.service";
import { Utilisateur } from "src/app/Model/Utilisateur";
@Component({
  selector: "app-analytics",
  templateUrl: "./analytics.component.html",
  styleUrls: ["./analytics.component.sass"],
})
export class AnalyticsComponent implements OnInit {
  faAngleDown = faAngleDown;
  faAngleUp = faAngleUp;
  faTh = faTh;
  faCheck = faCheck;
  faTrash = faTrash;
  faCalendarAlt = faCalendarAlt;

  heading = "Dashboard Produit";
  subheading = "";
  icon = "pe-7s-display1 icon-gradient bg-tempting-azure";

  slideConfig6 = {
    className: "center",
    infinite: true,
    slidesToShow: 1,
    speed: 500,
    adaptiveHeight: true,
    dots: true,
  };
  public meilleur: Produit[] = [];
  public mauvais: Produit[] = [];

  nombreProduits: any;
  nombreCommerciaux: any;
  nombreCommandes: any;
  constructor(
    private modalService: NgbModal,
    private produitService: ProduitService,
    private userService: UtilisateurService,
    private cmdService: CommandeService,
    private http: HttpClient
  ) {
    //   this.produits = [];
    this.getMeilleurProduits();
    this.getMauvaisProduits();
  }
  public getMeilleurProduits(): void {
    this.produitService.getMeilleur().subscribe(
      (response: Produit[]) => {
        this.meilleur = response;
        console.log(this.meilleur);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getNombreCommerciaux(): void {
    this.userService.getCountComm().subscribe(
      (data) => {
        this.nombreCommerciaux = data;
        console.log(this.nombreCommerciaux);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getNombreCommandes(): void {
    this.cmdService.getCount().subscribe(
      (data) => {
        this.nombreCommandes = data;
        console.log(this.nombreCommandes);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getNombreProduits(): void {
    this.produitService.getCount().subscribe(
      (data) => {
        this.nombreProduits = data;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getMauvaisProduits(): void {
    this.produitService.getMauvais().subscribe(
      (response: Produit[]) => {
        this.mauvais = response;
        console.log(this.mauvais);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  ngOnInit() {
    this.getMeilleurProduits();
    this.getMauvaisProduits();
    this.getNombreProduits();
    this.getNombreCommandes();
    this.getNombreCommerciaux();
  }
}
