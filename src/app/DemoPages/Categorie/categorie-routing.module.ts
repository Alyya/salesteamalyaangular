import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Categorie",
      status: "false",
    },
    children: [
      {
        path: "ajouterCategorie",
        loadChildren: () =>
          import("../Categorie/add-categorie/add-categorie.module").then(
            (m) => m.AddCategorieModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategorieRoutingModule {}
