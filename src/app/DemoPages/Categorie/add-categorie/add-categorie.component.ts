import { CategorieService } from "./../../../Service/categorie.service";
import { Categorie } from "./../../../Model/Categorie";
import { Component, OnInit } from "@angular/core";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { faTags } from "@fortawesome/free-solid-svg-icons";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { HttpErrorResponse } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";

import {
  NgForm,
  FormGroup,
  AbstractControlOptions,
  FormBuilder,
  Validators,
} from "@angular/forms";

@Component({
  selector: "app-add-categorie",
  templateUrl: "./add-categorie.component.html",
  styleUrls: ["./add-categorie.component.sass"],
})
export class AddCategorieComponent implements OnInit {
  heading = "Modals";
  subheading =
    "Wide selection of modal dialogs styles and animations available.";
  icon = "pe-7s-phone icon-gradient bg-premium-dark";

  closeResult: string;

  toggleMobileSidebar: any;
  faStar = faStar;
  faPlus = faPlus;
  faAngleDown = faAngleDown;
  faSearch = faSearch;
  faTags = faTags;
  faCalendarAlt = faCalendarAlt;

  //-----------------//
  public categories: Categorie[] = [];
  //-----------------//
  form!: FormGroup;

  categorieDetails = null;
  categorieToUpdate = {
    idCategorie: 0,
    nomCategorie: "",
    description: "",
  };
  searchText;

  constructor(
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private categorieService: CategorieService,
    private toastr: ToastrService
  ) {
    this.categories = [];
    this.getCategories();
  }
  open(content) {
    this.modalService.open(content).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {
    this.getCategories();
  }
  public getCategories(): void {
    this.categorieService.getCategories().subscribe(
      (response: Categorie[]) => {
        this.categories = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchCategories(key: string): void {
    console.log(key);
    const results: Categorie[] = [];
    for (const categorie of this.categories) {
      if (
        categorie.nomCategorie.toLowerCase().indexOf(key.toLowerCase()) !== -1
      ) {
        results.push(categorie);
      }
    }
    this.categories = results;
    if (results.length === 0 || !key) {
      this.getCategories();
    }
  }
  public onAddCategorie(addForm: NgForm): void {
    document.getElementById("add-categorie-form")?.click();
    console.log(addForm.value);
    this.categorieService.addCategorie(addForm.value).subscribe(
      (response: Categorie) => {
        console.log(response);
        this.getCategories();
        addForm.reset();
        this.toastr.success("New row added successfully", "New Row");
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }
  deleteCategorie(categorie: Categorie) {
    this.categorieService.deleteCategorie(categorie.idCategorie).subscribe(
      (response) => {
        console.log(response);
        this.getCategories();
      },
      (error) => {
        console.log(error);
      }
    );
  }
  edit(categorie: Categorie) {
    this.categorieToUpdate = categorie;
  }
  public updateCategorie() {
    this.categorieService.updateCategorie(this.categorieToUpdate).subscribe(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
