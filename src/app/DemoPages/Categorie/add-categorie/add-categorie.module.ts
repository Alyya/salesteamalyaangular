import { FormsModule } from "@angular/forms";
import { AddCategorieComponent } from "./add-categorie.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AddCategorieRoutingModule } from "./add-categorie-routing.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [AddCategorieComponent],
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule,
    NgbModule,
    AddCategorieRoutingModule,
  ],
})
export class AddCategorieModule {}
