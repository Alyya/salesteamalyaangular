import { Commande } from "./../../Model/Commande";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Commande",
      status: "false",
    },
    children: [
      {
        path: "validerCmd",
        loadChildren: () =>
          import("../Commande/valider-cmd/valider-cmd.module").then(
            (m) => m.ValiderCmdModule
          ),
      },
      {
        path: "listCmd",
        loadChildren: () =>
          import("../Commande/list-cmd/list-cmd.module").then(
            (m) => m.ListCmdModule
          ),
      },
      {
        path: "detailCmd",
        loadChildren: () =>
          import("../Commande/detail-cmd/detail-cmd.module").then(
            (m) => m.DetailCmdModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommandeRoutingModule {}
