import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValiderCmdComponent } from './valider-cmd.component';

describe('ValiderCmdComponent', () => {
  let component: ValiderCmdComponent;
  let fixture: ComponentFixture<ValiderCmdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValiderCmdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValiderCmdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
