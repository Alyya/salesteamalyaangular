import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ValiderCmdRoutingModule } from "./valider-cmd-routing.module";

@NgModule({
  declarations: [],
  imports: [CommonModule, ValiderCmdRoutingModule],
})
export class ValiderCmdModule {}
