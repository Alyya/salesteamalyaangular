import { ValiderCmdComponent } from "./valider-cmd.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: ValiderCmdComponent,
    data: {
      title: "ValiderCmd",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ValiderCmdRoutingModule {}
