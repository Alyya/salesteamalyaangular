import { CommandeService } from "./../../../Service/commande.service";
import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Commande } from "src/app/Model/Commande";

@Component({
  selector: "app-valider-cmd",
  templateUrl: "./valider-cmd.component.html",
  styleUrls: ["./valider-cmd.component.sass"],
})
export class ValiderCmdComponent implements OnInit {
  public cmds: Commande[] = [];

  constructor(
    private modalService: NgbModal,
    private commandeService: CommandeService
  ) {}
  closeResult: string;
  open(content) {
    this.modalService.open(content).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  public getCommandNonValider(): void {
    this.commandeService.getCmds().subscribe(
      (response: Commande[]) => {
        this.cmds = response;
        console.log(this.cmds);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  ngOnInit(): void {
    this.getCommandNonValider();
  }
}
