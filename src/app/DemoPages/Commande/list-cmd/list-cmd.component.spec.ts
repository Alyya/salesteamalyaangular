import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCmdComponent } from './list-cmd.component';

describe('ListCmdComponent', () => {
  let component: ListCmdComponent;
  let fixture: ComponentFixture<ListCmdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListCmdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCmdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
