import { ListCmdComponent } from "./list-cmd.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
const routes: Routes = [
  {
    path: "",
    component: ListCmdComponent,
    data: {
      title: "ListCommande",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListCmdRoutingModule {}
