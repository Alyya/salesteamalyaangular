import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ListCmdRoutingModule } from "./list-cmd-routing.module";

@NgModule({
  declarations: [],
  imports: [CommonModule, ListCmdRoutingModule],
})
export class ListCmdModule {}
