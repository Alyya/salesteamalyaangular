import { DetailCmdComponent } from "./detail-cmd.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: DetailCmdComponent,
    data: {
      title: "Detail-Commande",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailCmdRoutingModule {}
