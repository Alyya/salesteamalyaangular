import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DetailCmdRoutingModule } from "./detail-cmd-routing.module";

@NgModule({
  declarations: [],
  imports: [CommonModule, DetailCmdRoutingModule],
})
export class DetailCmdModule {}
