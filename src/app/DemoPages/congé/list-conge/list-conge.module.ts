import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListCongeRoutingModule } from './list-conge-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ListCongeRoutingModule
  ]
})
export class ListCongeModule { }
