import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CongéRoutingModule } from './congé-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CongéRoutingModule
  ]
})
export class CongéModule { }
