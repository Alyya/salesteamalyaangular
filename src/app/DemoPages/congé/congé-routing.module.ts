import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Congé",
      status: "false",
    },
    children: [
      {
        path: "listConge",
        loadChildren: () =>
          import("../congé/list-conge/list-conge.module").then(
            (m) => m.ListCongeModule
          ),
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CongéRoutingModule {}
