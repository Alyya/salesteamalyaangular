import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Client",
      status: "false",
    },
    children: [
      {
        path: "listeClients",
        loadChildren: () =>
          import("../Clients/liste-clients/liste-clients.module").then(
            (m) => m.ListeClientsModule
          ),
      },
      {
        path: "localiserClients",
        loadChildren: () =>
          import("../Clients/localiser-client/localiser-client.module").then(
            (m) => m.LocaliserClientModule
          ),
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientsRoutingModule {}
