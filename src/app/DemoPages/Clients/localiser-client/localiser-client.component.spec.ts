import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocaliserClientComponent } from './localiser-client.component';

describe('LocaliserClientComponent', () => {
  let component: LocaliserClientComponent;
  let fixture: ComponentFixture<LocaliserClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocaliserClientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocaliserClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
