import { LocaliserClientComponent } from "./localiser-client.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: LocaliserClientComponent,
    data: {
      title: "localiser-Clients",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LocaliserClientRoutingModule {}
