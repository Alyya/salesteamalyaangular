import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocaliserClientRoutingModule } from './localiser-client-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LocaliserClientRoutingModule
  ]
})
export class LocaliserClientModule { }
