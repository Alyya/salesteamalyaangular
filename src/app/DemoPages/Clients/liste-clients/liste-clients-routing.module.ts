import { ListeClientsComponent } from "./liste-clients.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: ListeClientsComponent,
    data: {
      title: "List-Clients",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListeClientsRoutingModule {}
