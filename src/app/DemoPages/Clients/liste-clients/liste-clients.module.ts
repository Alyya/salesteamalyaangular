import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListeClientsRoutingModule } from './liste-clients-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ListeClientsRoutingModule
  ]
})
export class ListeClientsModule { }
