import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListFactureRoutingModule } from './list-facture-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ListFactureRoutingModule
  ]
})
export class ListFactureModule { }
