import { CommandeService } from "./../../../Service/commande.service";
import { Component, OnInit } from "@angular/core";
import { Color } from "ng2-charts/ng2-charts";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { faAngleUp } from "@fortawesome/free-solid-svg-icons";
import { faTh } from "@fortawesome/free-solid-svg-icons";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { Commande } from "src/app/Model/Commande";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: "app-list-facture",
  templateUrl: "./list-facture.component.html",
  styleUrls: ["./list-facture.component.sass"],
})
export class ListFactureComponent implements OnInit {
  faAngleDown = faAngleDown;
  faAngleUp = faAngleUp;
  faTh = faTh;
  faCheck = faCheck;
  faTrash = faTrash;
  faCalendarAlt = faCalendarAlt;

  heading = "Analytics Dashboard";
  subheading =
    "This is an example dashboard created using build-in elements and components.";
  icon = "pe-7s-news-paper icon-gradient bg-tempting-azure";

  public cmd: Commande[] = [];

  constructor(private commandeService: CommandeService) {}

  ngOnInit(): void {
    this.getCommandes();
  }
  public getCommandes(): void {
    this.commandeService.getCmds().subscribe(
      (response: Commande[]) => {
        this.cmd = response;
        console.log(this.cmd);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
}
