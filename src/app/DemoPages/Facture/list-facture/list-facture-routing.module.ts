import { ListFactureComponent } from "./list-facture.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: ListFactureComponent,
    data: {
      title: "List-Facture",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListFactureRoutingModule {}
