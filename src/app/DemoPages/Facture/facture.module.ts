import { PageTitleModule } from "./../../Layout/Components/page-title/page-title.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FactureRoutingModule } from "./facture-routing.module";
PageTitleModule;
@NgModule({
  declarations: [],
  imports: [CommonModule, FactureRoutingModule, PageTitleModule],
})
export class FactureModule {}
