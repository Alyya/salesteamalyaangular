import { DetailFactureComponent } from "./detail-facture.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: DetailFactureComponent,
    data: {
      title: "Detail-Facture",
    },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailFactureRoutingModule {}
