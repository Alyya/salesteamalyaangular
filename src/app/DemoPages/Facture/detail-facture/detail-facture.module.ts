import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailFactureRoutingModule } from './detail-facture-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DetailFactureRoutingModule
  ]
})
export class DetailFactureModule { }
