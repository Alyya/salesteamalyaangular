import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Facture",
      status: "false",
    },
    children: [
      {
        path: "listeFacture",
        loadChildren: () =>
          import("./list-facture/list-facture.module").then(
            (m) => m.ListFactureModule
          ),
      },
      {
        path: "detailFacture",
        loadChildren: () =>
          import("./detail-facture/detail-facture.module").then(
            (m) => m.DetailFactureModule
          ),
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FactureRoutingModule {}
